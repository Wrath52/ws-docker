FROM node:11.15.0-stretch
RUN mkdir -p /opt/app
COPY ./ /opt/app
RUN apt-get update
RUN apt-get install nano 
RUN apt-get install nginx --yes
COPY ./nginx.conf /etc/nginx/sites-available/default
WORKDIR /opt/app
EXPOSE 80
CMD service nginx configtest && service nginx start && node index.js
