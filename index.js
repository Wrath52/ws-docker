var WebSocketServer = new require("ws");

// подключённые клиенты
var clients = {};

// WebSocket-сервер на порту 8081
var webSocketServer = new WebSocketServer.Server({
  port: 8081,
});

webSocketServer.on("connection", function (ws) {
  var id = Math.random().toString(36).replace("0.", "");

  clients[id] = ws;
  console.log(`Open - ${new Date().toISOString()} - ${id} - ${ws._protocol}`);

  ws.on("message", function (message) {
    console.log("получено сообщение " + message);

    for (var key in clients) {
      clients[key].send(message);
    }
  });

  ws.on("close", function () {
    console.log(`Close - ${new Date().toISOString()} - ${id} - ${ws._protocol}`);
    delete clients[id];
  });
});

console.log('Сервер ожидает подключений');